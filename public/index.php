<?php

require __DIR__ . '/../vendor/autoload.php';

use App\Models\Task;
use App\Storage\MySqlDatabaseTaskStorage;
use App\TaskManager;

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$task = new Task;

$host = getenv('DATABASE_SERVER');
$dbname = getenv('DATABASE_NAME');
$user = getenv('DATABASE_USER');
$password = getenv('DATABASE_PASSWORD');

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = $pdo->query("SELECT * FROM tasks");
} catch (PDOException $e) {
    echo $e->getMessage();
}

$storage = new MySqlDatabaseTaskStorage($pdo);

// Get with id
$storage->get(1);

// Get all
$storage->all();

// Store new row
$task->setDescription('Go to Webstart')->setDue(new DateTime('+ 3 days'))->setComplete(true);
$storage->store($task);

// Update last inserted row
$task->setDescription('Go to Webstart Modified')->setDue(new DateTime('+ 25 days'))->setComplete(true);
$storage->update($task);



