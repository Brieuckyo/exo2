<?php

namespace App\Storage;

use App\Models\Task;
use App\Storage\Contracts\TaskStorageInterface;

class MySqlDatabaseTaskStorage implements TaskStorageInterface
{

    protected $pdo;
    protected $lastIsertId;

    /**
     * MySqlDatabaseTaskStorage constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param Task $task
     */
    public function store(Task $task)
    {
        try {
            $description = $task->getDescription();
            $date = $task->getDue();
            $due = $date->format('Y/m/d h:i:s');
            $complete = $task->getComplete();
            $this->pdo->query("INSERT INTO tasks (description, due, complete) VALUES ('{$description}', '{$due}',  '{$complete}')");
            $this->lastIsertId = $this->pdo->lastInsertId();

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param Task $task
     */
    public function update(Task $task)
    {
        try {
            $description = $task->getDescription();
            $date = $task->getDue();
            $due = $date->format('Y/m/d h:i:s');
            $complete = $task->getComplete();
            $this->pdo->query("UPDATE tasks SET description = '{$description}', due = '{$due}', complete = '{$complete}' WHERE id = {$this->lastIsertId}");
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $id
     */
    public function get($id)
    {
        $task = $this->pdo->query("SELECT * FROM tasks WHERE id = {$id}");
        if (is_array($task) || is_object($task)) {
            echo "<ul>";
            foreach ($task as $t) {
                echo "<li>";
                echo $t['id'] . ' ' . $t['description'] . ' ' . $t['due'];
                echo "</li>";
            }
            echo "</ul>";
        }
    }

    public function all()
    {
        $all = $this->pdo->query('SELECT * FROM tasks');
        echo "<ul>";
        foreach ($all as $task) {
            echo "<li>";
            echo $task['id'] . ' ' . $task['description'] . ' ' . $task['due'] . ' ' . $task['complete'];
            echo "</li>";
        }
        echo "</ul>";
    }
}